# Containerized BIND ![Docker Build Status](https://img.shields.io/docker/build/aechelon/bind.svg) 
This repository is the home of a small BIND Docker image which offers a variety of features out of the box.

# Features
* Master / Slave / Resolver mode configurable
* Easy configuration and customization through environment variables and custom file mounts
* Auto-creation of DNSSEC key files and auto-signing zone files
* Small and fast
* Alpine based

# Version
You can check the current version of BIND [here](https://pkgs.alpinelinux.org/package/edge/main/x86_64/bind).
(Sorry, I do not make any version pinnings.)

# How to use
You can dive right in with:
```
docker run --detach --name bind aechelon/bind
```
This container will not do much but you can test its resolver capabilities with `docker exec bind dig google.com @localhost`. The first query should take some time to resolve but if you do a second query you will see the lightning speed resolve time BIND can provide.

### Master Mode
You need to provide some zone files and if DNSSEC is used some DNSSEC domains for the image to create the necessary keys. The zone files are mounted into the container from either a host path or a persistent volume. It's easier to use a host path when you edit your files from time to time. Make sure you name your zone file `DOMAIN.zone` (example\.com.zone for example). In this example we will use a single domain with DNSSEC:
```
docker volume create bind-keys
docker run --detach --name bind-master --env BIND_MODE=master --env DNSSEC_DOMAINS="example.com" --env SLAVES="199.43.133.53 2001:500:8d::53" --mount type=bind,source=$PWD/zones/,target=/var/bind/zones/ --mount type=volume,source=bind-keys,target=/var/bind/dnssec/keys/ --publish 53:53/tcp --publish 53:53/udp aechelon/bind
```

### Slave Mode
You need to define the mode and the ip addresses of your master server:
```
docker run --detach --name bind-slave --env BIND_MODE=slave --env MASTERS="199.43.135.53 2001:500:8f::53" aechelon/bind
```

### Resolver Mode
To make the most out of your very own resolver create a Docker network first where you put the container in so you can connect other containers to it. This is useful for services that make heavy use of DNS queries. Do not forget that the default mode is `resolver` so you do not need to define `BIND_MODE` here.
```
docker network create --internal --subnet 172.22.0.0/16 resolver
docker create --name bind-resolver --env TRUSTED_NETWORKS="172.22.0.0/16" aechelon/bind
docker network connect resolver bind-resolver
docker run --rm --network resolver --entrypoint "" aechelon/bind dig google.com @bind-resolver
```

# Environment variables
| Variable | Type | Purpose |
| -------  | ---- | ------- |
| DNSSEC_DOMAINS | String [space separated domains] | A list of domains for which a set of keys should be created in order to use DNSSEC. Key creation is only triggered when this variable is defined. |
| DNSSEC_RECREATE_KEYS | Boolean [true/false] | Delete and re-create all keys (ZSK & KSK) for all domains given by DNSSEC_DOMAINS. |
| DNSSEC_RECREATE_ZSK | Boolean [true/false] | Delete and re-create ZSKs only for domains given by DNSSEC_DOMAINS. |
| DNSSEC_KEY_ALGORITHM | String [[See option -a of dnssec-keygen](https://ftp.isc.org/isc/bind9/cur/9.9/doc/arm/man.dnssec-keygen.html)] | The algorithm to use when creating ZSKs and KSKs (defaults to NSEC3RSASHA1 when not defined). |
| DNSSEC_KEY_SIZE | Integer [[See option -b of dnssec-keygen](https://ftp.isc.org/isc/bind9/cur/9.9/doc/arm/man.dnssec-keygen.html)] | The key size for the algorithm when creating keys (defaults to 2048 when not defined). |
| BIND_MODE | String [master/slave/resolver] | Sets the target state of BIND. Defaults to resolver. |
| TRUSTED_NETWORKS | String [space separated subnets in CIDR notation] | The subnets given are added to the internal trusted ACL for which access is granted to query the server (allow-query and allow-recursion). Do not use publicly routable addresses unless you want a world-accessable DNS resolver for everyone to use. If you want to mount your own ACL file, you can replace the file /etc/bind/named.conf.acl via Dockers --mount option. In this case you do not need to declare this env variable. |
| MASTERS (enumerated when BIND_MODE=slave) | String [space separated ip addresses] | The IP addresses given here are added to the named.conf.zones file to the [masters directive](http://www.zytrax.com/books/dns/ch7/zone.html#masters) when enumerating all zone files in the directory /var/bind/zones/. |
| SLAVES (enumerated when BIND_MODE=master) | String [space separated ip addresses] | The IP addresses given here are added to the named.conf.zones file to the [allow-transfer directive](www.zytrax.com/books/dns/ch7/xfer.html#allow-transfer) when enumerating all zone files in the directory /var/bind/zones/. |
| OWN_ZONE_CONFIG | Boolean [true/false] | The image compiles the zone config file with the zone files found at /var/bind/zones/. Set this to true if you provide your own named.conf.zones zone config file so it is left untouched. |

# Pre-defined directory
This image has a special directory structure where config files and user given files are located. If you mount your own files into this image, make sure you mount them as read-only. The image is smart enough to avoid file modifications on these files.

- /etc/bind/ - All config files during BIND initialization reside here. User given files can replace image files wherever necessary.
  - /etc/bind/named.conf - BIND default configuration file. You can replace this file if you have a single config file and do not need any auxillary files in the image.
  - /etc/bind/named.conf.options - BIND global options file. You can replace this file if you need special config options not provided by this image.
  - /etc/bind/named.conf.acl - BIND ACL file used for query and recursion access restriction. Use TRUSTED_NETWORKS if you need to add networks to the default ACL. If you need to declare additional ACLs then mount them as named.conf.acl.aux. The image will add this file to named.conf.acl.
  - /etc/bind/named.conf.zones - BIND zone config file. Like the previous files you can also replace this file if you need a more complex configuration. Use `OWN_ZONE_CONFIG` to avoid file manipulation by the image.
- /var/bind/ - Run related files reside here.
  - /var/bind/zones/ - Directory where user defined zones files are kept. You need to mount something here unless you use resolver mode. Make sure the owner of the source file is uid `100` and gid `101` or world readable (at least file permission `0444`) so BIND can read your files. Also avoid mounting them in read-only mode because BIND writes journal files for its dynamic updates. In order to recognize your files name your zone file `DOMAIN.zone`. You can create the container once and restart it every time you made changes in your zone files to get reflected.
  - /var/bind/dnssec/ - All DNSSEC related stuff (keys and signed zone files + some additional metadata) is stored here.
    - /var/bind/dnssec/zones/ - Signed zone files are here. These files are recreated at container startup.
    - /var/bind/dnssec/keys/ - DNSSEC keys are stored here. If you change your DNSSEC secured domains or need to recreate your keys you need to create a new container.

# Persistent directories
Use a persistent volume or host path to make sure these files will survive a container removal.

- /var/bind/dnssec/keys/ - You will only need your keys when you use DNSSEC. Signed zone files and some metadata files are created on initial container startup.
