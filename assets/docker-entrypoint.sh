#!/bin/bash
set -e

error() {
    echo "Error on line $1 in file docker-entrypoint.sh"
}

trap 'error $LINENO' ERR

isElementIn () {
    local e match="$1"
    shift
    for e; do [[ "$e" == "$match" ]] && return 0; done
    return 1
}

KEYS_DIRECTORY=/var/bind/dnssec/keys
ZONES_DIRECTORY=/var/bind/zones
SIGNED_ZONES_DIRECTORY=/var/bind/dnssec/zones

if [ -n "$BIND_MODE" ]; then
    _mode=$(echo "$BIND_MODE" | tr '[:upper:]' '[:lower:]')
else
    _mode="resolver"
fi

if [ ! -f /initialized ]; then
    if [ ! -d "$ZONES_DIRECTORY" ]; then
        mkdir -p "$ZONES_DIRECTORY"
    fi

    case "$_mode" in
        'master')
            # Useful when you use a new algorithm or just want to throw out old keys
            if [ "$DNSSEC_RECREATE_KEYS" = "true" ]; then
                rm -rf "$KEYS_DIRECTORY"
            fi

            KEY_ALGORITHM=${DNSSEC_KEY_ALGORITHM:-NSEC3RSASHA1}
            KEY_SIZE=${DNSSEC_KEY_SIZE:-2048}

            if [ -n "$DNSSEC_DOMAINS" ]; then
                if [ ! -d "$KEYS_DIRECTORY" ]; then
                    mkdir -p $KEYS_DIRECTORY
                    chown -R named:named $KEYS_DIRECTORY
                fi
                mkdir -p $SIGNED_ZONES_DIRECTORY
                chown named:named $SIGNED_ZONES_DIRECTORY

                # Add keys directory to BIND options unless it's user mounted
                if [ -w /etc/bind/named.conf.options ]; then
                    sed -E "s|^\s+directory \"/var/bind/\";$|&\n    key-directory \"/var/bind/keys/\";|" -i /etc/bind/named.conf.options
                fi

                for domain in ${DNSSEC_DOMAINS[*]}; do
                    _domaindir=${KEYS_DIRECTORY}/${domain}

                    # If domain keys directory does not exist yet, create keys and adjust permissions
                    if [ ! -d $_domaindir ]; then
                        mkdir -p $_domaindir
                        chown -R root:named $_domaindir
                        dnssec-keygen -a NSEC3RSASHA1 -b 2048 -n ZONE -K $_domaindir $domain
                        dnssec-keygen -f KSK -a NSEC3RSASHA1 -b 4096 -n ZONE -K $_domaindir $domain
                    else
                        # Check for pre-existing keys
                        _zsk=0
                        _ksk=0
                        for key in ${_domaindir}/K${domain}*.key; do
                            case "$(cat $key | grep -oE "DNSKEY ([0-9]{3})" | cut -d ' ' -f 2)" in
                                256)
                                    if [ "$DNSSEC_RECREATE_ZSK" = "true" ]; then
                                        rm "$key"
                                        rm "${key/.key/.private}"
                                    else
                                        _zsk=1
                                    fi
                                    ;;
                                257)
                                    _ksk=1
                                    ;;
                            esac
                        done

                        # Regenerate missing keys
                        if [ $_zsk -eq 0 ]; then
                            dnssec-keygen -a $KEY_ALGORITHM -b $KEY_SIZE -n ZONE -K $_domaindir $domain
                        fi
                        if [ $_ksk -eq 0 ]; then
                            dnssec-keygen -f KSK -a $KEY_ALGORITHM -b $KEY_SIZE -n ZONE -K $_domaindir $domain
                        fi
                    fi

                    if [ -f ${ZONES_DIRECTORY}/${domain}.zone ]; then
                        if [ "$KEY_ALGORITHM" = "NSEC3RSASHA1" ]; then
                            _salt="-3 $(head -c 256 /dev/random | sha1sum | head -c 16)"
                        else
                            _salt=""
                        fi
                        dnssec-signzone -S $_salt -A -N INCREMENT -K ${KEYS_DIRECTORY}/${domain}/ -d ${SIGNED_ZONES_DIRECTORY} -o $domain -f $SIGNED_ZONES_DIRECTORY/${domain}.zone.signed -t /var/bind/zones/${domain}.zone
                    else
                        # We have a domain for which DNSSEC keys should be created but no zone file, strange...
                        echo "ERROR: DNSSEC domain $domain given but no corresponding zone file found"
                        exit 1
                    fi

                    # You must publish these KSK hashes to your domain registrar
                    for file in ${SIGNED_ZONES_DIRECTORY}/dsset-*; do
                        _domain=$(basename $file | cut -d '-' -f 2 | sed 's/.$//')
                        echo "### START OF DS RECORDS FOR $_domain ###"
                        cat $file
                        echo "### ENF OF DS RECORDS FOR $_domain ###"
                    done
                done
            fi

            if [ "$OWN_ZONE_CONFIG" != 'true' ]; then
                for zone in ${ZONES_DIRECTORY}/*.zone; do
                    _file=$(basename $zone)
                    _domain=$(basename $zone | rev | cut -d '.' -f 2- | rev)
                    isElementIn "$_domain" "${DNSSEC_DOMAINS[@]}" && :
                    if [ "$?" -eq 0 ]; then
                        cat <<EOF >> /etc/bind/named.conf.zones
zone "${_domain}" IN {
    type master;
    file "${SIGNED_ZONES_DIRECTORY}/${_file}.signed";
    key-directory "${KEYS_DIRECTORY}/${_domain}/";
    auto-dnssec maintain;
    update-policy local;
    allow-transfer { ${SLAVES// /; }; };
    allow-query { any; };
};
EOF
                    else
                        cat <<EOF >> /etc/bind/named.conf.zones
zone "${_domain}" IN {
    type master;
    file "${ZONES_DIRECTORY}/${_file}";
    update-policy local;
    allow-transfer { ${SLAVES// /; }; };
    allow-query { any; };
};
EOF
                    fi
                done
            fi
            ;;
        'slave')
            if [ -z "${MASTERS##*|*}" ]; then
                for master in ${MASTER[*]}; do
                    _domain=$(echo $master | cut -d '|' -f 1)
                    _addr=$(echo $master | cut -d '|' -f 2-)

                    cat <<EOF >> /etc/bind/named.conf.zones
zone "${_domain}" {
    type slave;
    masters { ${_addr//|/; }; };
    file "/var/bind/zones/${_domain}.zone";
};
EOF
                done
            else
                for zone in ${ZONES_DIRECTORY}/*.zone; do
                    _domain=$(basename $zone | rev | cut -d '.' -f 2- | rev )
                    cat <<EOF >> /etc/bind/named.conf.zones
zone "${_domain}" {
    type slave;
    masters { ${MASTERS// /; }; };
    file "/var/bind/zones/${_domain}.zone";
};
EOF
                done
            fi
            ;;
        'resolver')
            for network in ${TRUSTED_NETWORKS[*]}; do
                sed "s|^acl trusted-internal {$|&\n\t${network};|" -i /etc/bind/named.conf.acl
            done
            ;;

    esac

    # Create key file for rndc usage
    # You can use rndc to debug issues with BIND or to resign and reload the zones
    # You can use rndc only when inside the container (it is bound to localhost as a safety measure) 
    dnssec-keygen -a HMAC-SHA256 -b 256 -n HOST -K /tmp/ rndc
    _keysecret=$(cat /tmp/Krndc*.key | cut -d " " -f 7)
    rm /tmp/Krndc*
    touch /etc/bind/rndc.key
    echo "key \"rndc-key\" { algorithm hmac-sha256; secret \"${_keysecret}\"; };" >> /etc/bind/rndc.key

    if [ -w "/etc/bind/named.conf" ]; then
        echo "controls { inet 127.0.0.1 allow { localhost; } keys { \"rndc-key\"; }; };" >> /etc/bind/named.conf
        echo "include \"/etc/bind/rndc.key\";" >> /etc/bind/named.conf
    fi

    if [ -f "/etc/bind/named.conf.acl.aux" ]; then
        cat /etc/bind/named.conf.acl.aux >> /etc/bind/named.conf.acl
    fi

    # Download latest list of Root Named Server
    wget ftp://ftp.rs.internic.net/domain/db.cache -O /var/bind/named.ca

    chown -fR root:root /etc/bind/ || true
    chmod -fR 755 /etc/bind/ || true
    chown named:root /etc/bind/rndc.key
    chmod 640 /etc/bind/rndc.key

    
    # Mark container as initialized
    touch /initialized
else
    case "$BIND_MODE" in
        'master')
            for domain in ${DNSSEC_DOMAINS[*]}; do
                _domaindir=${KEYS_DIRECTORY}/${domain}

                if [ -f ${ZONES_DIRECTORY}/${domain}.zone ]; then
                    if [ "$KEY_ALGORITHM" = "NSEC3RSASHA1" ]; then
                        _salt="-3 $(head -c 256 /dev/random | sha1sum | head -c 16)"
                    else
                        _salt=""
                    fi
                    dnssec-signzone -S $_salt -A -N INCREMENT -K ${KEYS_DIRECTORY}/${domain}/ -d ${SIGNED_ZONES_DIRECTORY} -o $domain -f $SIGNED_ZONES_DIRECTORY/${domain}.zone.signed -t /var/bind/zones/${domain}.zone
                else
                    # We have a domain for which DNSSEC keys should be created but no zone file, strange...
                    echo -e "WARNING: DNSSEC domain $domain given but no corresponding zone file found!\nThink about replacing this container with a new one."
                fi
            done
            ;;
    esac
fi

# Fix permissions
# Some files may be marked as read-only by the user (file mounts), in this case
# just skip over these files and mark the operation as successful
if [ "$BIND_MODE" = 'slave' ]; then
    chown -fR named:named "$ZONES_DIRECTORY" || true
    chmod -fR 750 "$ZONES_DIRECTORY" || true
fi
if [ "$BIND_MODE" = 'master' ] && [ -n "$DNSSEC_DOMAINS" ]; then
    chown -R named:named "$KEYS_DIRECTORY"
fi
if [ -d "$SIGNED_ZONES_DIRECTORY" ]; then
    chown -R named:named $SIGNED_ZONES_DIRECTORY
    chmod -R 750 $SIGNED_ZONES_DIRECTORY
fi

exec "$@"
